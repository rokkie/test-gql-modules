import 'graphql-import-node';
import * as http from 'http';
import express from 'express';
import * as portfinder from 'portfinder';
import { ApolloServer } from 'apollo-server';
import { ApolloServer as ApolloServerExpress } from 'apollo-server-express';
import * as modules from './modules';
import { ApolloGateway } from '@apollo/gateway';
import { buildFederatedSchema } from '@apollo/federation';

const GQL_PORT = 3000;
const GQL_PATH = '/graphql';

const services = Object
  .values(modules)
  .map(async module => {
    const port         = await portfinder.getPortPromise(),
          apolloServer = new ApolloServer({
            context: session => session,
            schema : buildFederatedSchema(module),
          });

    return apolloServer
      .listen({ port, path: GQL_PATH })
      .then(info => {
        console.log(`Module '${module.name}' is listening on ${info.url}`);

        return {
          url : info.url,
          name: module.name,
        };
      });
  });

Promise
  .all(services)
  .then(serviceList => {
    const app          = express(),
          httpServer   = http.createServer(app),
          apolloServer = new ApolloServerExpress({
            subscriptions: false,
            gateway      : new ApolloGateway({
              serviceList,
            }),
          });

    apolloServer.applyMiddleware({
      app,
      path: GQL_PATH,
    });
    httpServer.on('error', () => {
      apolloServer.stop();
    });
    httpServer.listen(GQL_PORT, () => {
      console.log(`GraphQL is listening on http://localhost:${GQL_PORT}${apolloServer.graphqlPath}`);
    });
  });
