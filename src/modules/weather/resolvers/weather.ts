import { IForecastResponse } from '../model';

export default {
  Forecast: {
    description: (parent: IForecastResponse) => parent.text,
  },
};
