import * as path from 'path';
import { GraphQLModule } from '@graphql-modules/core';
import { loadResolversFiles, loadSchemaFiles } from 'graphql-toolkit';

export const WeatherModule = new GraphQLModule({
  name     : 'weather',
  typeDefs : loadSchemaFiles(path.resolve(__dirname, 'schema')),
  resolvers: loadResolversFiles(path.resolve(__dirname, 'resolvers')),
});
