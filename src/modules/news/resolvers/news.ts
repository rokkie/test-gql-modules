import { IBulletinResponse } from '../model';

export default {
  Bulletin: {
    description: (parent: IBulletinResponse) => parent.content,
  },
};
